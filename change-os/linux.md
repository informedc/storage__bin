## Changing OS


Installing ZSH using arch 
	
	sudo pacman -S zsh 	-- shell installation
	yay -S --noconfirm zsh-theme-powerlevel10k-git	  -- theme installation
	echo 'source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
	



 Chrome extensions
 
	ublock origin
	enhanced - h264ify
	block image - speed up internet (https://chrome.google.com/webstore/detail/image-blocker-ex%20-9000/eaabjlbddgjhjfplckkiglekpmonabcm)
	json pretty print - (https://chrome.google.com/webstore/detail/json-viewer-pro/eifflpmocdbdmepbjaopkkhbfmdgijcc)
	music mode youtube - (https://chrome.google.com/webstore/detail/abbpaepbpakcpipajigmlpnhlnbennna)
	undisposition racle fork


Apps

	sharik appimage
	Color pick gpick
	redshifter arch aur
	fish bash terminal


